FROM node:lts-alpine

RUN apk -U add sqlite

WORKDIR /app
COPY . .
RUN npm install

# Dont run as root
USER 1000

VOLUME /app/chat_archive

CMD ["node", "matrix-recorder.js", "/app/chat_archive"]
